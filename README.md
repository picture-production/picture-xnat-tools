# picture-xnat-tools
Public repository containing picture-specific extensions to [xnatpy](https://gitlab.com/radiology/infrastructure/xnatpy)
Repo to maintain picture_xnat.py centrally and make it easy to install. 

See https://gitlab.com/picture/0-0_commons/-/blob/master/protocols/OverviewXNATvariables.md (private) for details about the data structure and variable(names) on the picture-XNAT server.

## Installation:
```bash
pip install git+https://gitlab.com/picture-production/picture-xnat-tools.git
```


## Example usage:
```python
import xnat
from picture_xnat import picture_xnat
import getpass
xnatSes = xnat.connect(('https://xnat.bmia.nl',user=input('username'),password=getpass.getpass())

exp =  xnatSes.experiments['BMIAXNAT_E79495']
t1c = exp.selected_t1c
print(t1c)
#returns:        <MrScanData T1c (1600)>
t1c_nii = t1c.get_nii(filename='', dicom_dir='') #Download nifti

# Download registration:
registration = exp.selected_reg.get_reg(output_dir='', verbose=False)

print(registration)
# returns:      {'warp': '/tmp/tmp2erk3do8/3040127454440292/scans/REG2-masked_deformable/resources/REGISTRATION/files/rei_def_reg_v0_1Warp.nii.gz',
#                'affine': '/tmp/tmp2erk3do8/3040127454440292/scans/REG2-masked_deformable/resources/REGISTRATION/files/rei_def_reg_v0_0GenericAffine.mat',
#                'inv_warp': '/tmp/tmp2erk3do8/3040127454440292/scans/REG2-masked_deformable/resources/REGISTRATION/files/rei_def_reg_v0_1InverseWarp.nii.gz',
#                'transforms_to_mni': ['/tmp/tmp2erk3do8/3040127454440292/scans/REG2-masked_deformable/resources/REGISTRATION/files/rei_def_reg_v0_1Warp.nii.gz',
#                '/tmp/tmp2erk3do8/3040127454440292/scans/REG2-masked_deformable/resources/REGISTRATION/files/rei_def_reg_v0_0GenericAffine.mat'],
#                'mni': './common/MNI152_T1_1mm.nii.gz'}

# Apply registration to selected T1c:
import ants

t1c_mni = ants.apply_transforms(moving = ants.image_read(t1c_nii), fixed = ants.image_read(registration['mni']), 
                                transformlist= registration['transforms_to_mni'], interpolator= 'linear')


```

# Functionality
## Subjects:
xnat.mixin.SubjectData.preop_experiment = preop_experiment  
xnat.mixin.SubjectData.postop_experiment = postop_experiment  

## Experiments:
xnat.mixin.ExperimentData.selected_scan = get_sScan  
xnat.mixin.ExperimentData.selected_t1c = selected_t1c  
xnat.mixin.ExperimentData.selected_t1w = selected_t1w  
xnat.mixin.ExperimentData.selected_t2w = selected_t2w  
xnat.mixin.ExperimentData.selected_flr = selected_flr  
xnat.mixin.ExperimentData.selected_reg = selected_reg  
xnat.mixin.ExperimentData.segmentations = segmentations  
xnat.mixin.ExperimentData.exportobj = exportobj  
xnat.mixin.ExperimentData.is_preop = is_preop  
xnat.mixin.ExperimentData.is_postop = is_postop    

## Scans:
xnat.mixin.ImageScanData.get_nii = get_nii  
xnat.mixin.ImageScanData.get_reg = get_reg  
xnat.mixin.ImageScanData.parent_scan = parent_scan  
\\ xnat.mixin.ImageScanData.fields = fields  DEPRECATED: Now included in the xnat-py develop branch.  
xnat.mixin.ImageScanData.set_field = set_field \\ will be DEPCRECATED, consider using setting using `fields['name'] = Value`  

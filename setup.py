from distutils.core import setup

setup(
    name='picture_xnat',
    version='0.1dev',
    packages=['picture_xnat',],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.md').read(),
    install_requires=[
        "xnat @ git+https://gitlab.com/radiology/infrastructure/xnatpy.git@6aef23a3da1b234a60ccf1dedc3fbb751df96651",
        'SimpleITK',
        'numpy'
    ],
    
)

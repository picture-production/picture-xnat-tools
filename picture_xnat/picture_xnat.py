import xnat
import os
from urllib import request
from zipfile import ZipFile
from io import BytesIO as StringIO
import glob
import tempfile
import subprocess
import SimpleITK as sitk
import numpy as np
import shutil

import platform
os.makedirs('./common/',exist_ok=True)
if platform.system() == 'Windows':
    if not(os.path.isfile('./common/dcm2niix.exe')):
        url = request.urlopen('https://github.com/rordenlab/dcm2niix/releases/download/v1.0.20171215/dcm2niix_3-Jan-2018_win.zip')
        zipfile = ZipFile(StringIO(url.read())).extractall('./common/')
    dcm2niix = "./common/dcm2niix.exe"

elif platform.system() == 'darwin':
    if not(os.path.isfile('./common/dcm2niix')):
        url = request.urlopen('https://github.com/rordenlab/dcm2niix/releases/download/v1.0.20171215/dcm2niix_3-Jan-2018_mac.zip')
        zipfile = ZipFile(StringIO(url.read())).extractall('./common/')
    st = os.stat("./common/dcm2niix")
    os.chmod("./common/dcm2niix", st.st_mode | stat.S_IEXEC)
    dcm2niix = "./common/dcm2niix"
else:
    if not(os.path.isfile('./common/dcm2niix')):
        url = request.urlopen('https://github.com/rordenlab/dcm2niix/releases/download/v1.0.20171215/dcm2niix_3-Jan-2018_lnx.zip')
        zipfile = ZipFile(StringIO(url.read())).extractall('./common/')
    import stat

    st = os.stat("./common/dcm2niix")
    os.chmod("./common/dcm2niix", st.st_mode | stat.S_IEXEC)
    dcm2niix = "./common/dcm2niix"
#    os.chmod("./common/dcm2niix", 0o775)
   
    
def get_sScan(self,scanType):
    if scanType.lower() in [k.lower() for k in self.fields.keys()]:
        right_case_scanType = [k for k in self.fields.keys() if k.lower()==scanType.lower()][0]
        return self.scans[self.fields[right_case_scanType]]
    else:
        return None
@property
def selected_t1c(self):
    return get_sScan(self,'t1c')
@property
def selected_t1w(self):
    return get_sScan(self,'t1w')
@property
def selected_t2w(self):
    return get_sScan(self,'t2w')
@property
def selected_flr(self):
    return get_sScan(self,'flr')
@property
def selected_reg(self):
    if 'reg' in self.fields.keys():
        return self.scans[self.fields['reg']]
    else:
        REG_ID=[scan.id for scan in self.scans.values() if scan.id in ['REG'+str(i) for i in range(30,0,-1)]]
        REG_ID.sort()
        if REG_ID:
            REG_ID=REG_ID[-1]
            return self.scans[REG_ID]
        else:
            return None

@property
def segmentations(self):
    return [s for s in self.scans.values() if s.type in ['ENT', 'ENR', 'ENF','ENT','ENR','CAV','NET','NCT','NER','NCR','ENP','NCP','NEP']]

@property
def autosegmentations(self):
    return [s for s in self.scans.values() if '_autoseg' in s.type]

@property
def parent_scan(self):
    if 'matching_scanID' in self.fields.keys():
        if self.fields['matching_scanID']:
            exp = self.xnat_session.experiments[self.data['image_session_ID']]
            return exp.scans[self.fields['matching_scanID']]
    return self.xnat_session.experiments[self.data['image_session_ID']].selected_t1c  

def exportobj(self):
    return [s for s in self.scans.values() if 'Export: Objects' in s.type]

def get_nii(self, filename='', dicom_dir='', segmentation = None, space = None, mask = None, verbose=False, overwrite=True):
    '''Downloads nifti to filename (default ./nii/{sessionID}-{scanID}-{scanType}.nii.gz
           space: [None, 'mni', 't1c']  - if specified downloads scan transformed to said space.
           segmentation: [True, False]  - if True: treat as segmentation (e.g. threshold Burned-IN Nifti if needed)
           mask: [True, False]          - if True: Download brain mask instead of image
           overwrite: [True, False]     - if False, use existing nifti file if already present.
    '''
    if not(filename):
        if space:
            filename = f'./nii-{space}/'+self.image_session_id + '-' + self.id + '-' + self.type
        else:
            filename = './nii/'+self.image_session_id + '-' + self.id + '-' + self.type
    
    filename = filename.replace(':','_').replace('.nii.gz', '')
    
    if os.path.isfile(filename+'.nii.gz') and not(os.path.isfile(filename+'a.nii.gz')) and not(overwrite):
        return filename+'.nii.gz'
    
    elif space:
        if 'SPACES' in self.resources:
            os.makedirs(os.path.split(os.path.abspath(filename+'.nii.gz'))[0],exist_ok=True)
            if space.lower() == 'mni':
                if self.id[:3] == 'SEG':
                    self.resources['SPACES'].files['MNI/'+'segmentation'+'.nii.gz'].download(filename+'.nii.gz', verbose = verbose)
                else:
                    if self.type.split('-')[0] in ['ENT', 'ENR', 'ENF','ENT','ENR','CAV','NET','NCT','NER','NCR','ENP','NCP','NEP']:
                        self.resources['SPACES'].files['MNI/'+self.type.split('-')[0]+'.nii.gz'].download(filename+'.nii.gz', verbose = verbose) 
                    else:
                        self.resources['SPACES'].files['MNI/'+self.type.split('-')[0].lower()+'.nii.gz'].download(filename+'.nii.gz', verbose = verbose) 
                if mask and 'MNI/'+(self.type).lower()+'_brain_mask.nii.gz' in self.resources['SPACES'].files:
                    filename_mask = filename + '-mask'
                    os.makedirs(os.path.split(os.path.abspath(filename+'.nii.gz'))[0],exist_ok=True)
                    self.resources['SPACES'].files['MNI/'+(self.type).lower()+'_brain_mask.nii.gz'].download(filename_mask+'.nii.gz', verbose = verbose)          
                    return filename+'.nii.gz', filename_mask+'.nii.gz'
                else:
                    return filename + '.nii.gz'
            if space.lower() == 't1c':
                os.makedirs(os.path.split(os.path.abspath(filename+'.nii.gz'))[0],exist_ok=True)
                self.resources['SPACES'].files['T1c/'+(self.type).lower()+'.nii.gz'].download(filename+'.nii.gz', verbose = verbose)
                return filename + '.nii.gz'
        else:
            print('no non-native spaces found')
            return
    
    elif 'NIFTI' in self.resources.keys():
        os.makedirs(os.path.split(os.path.abspath(filename+'.nii.gz'))[0],exist_ok=True)
        self.resources['NIFTI'].files[0].download(filename+'.nii.gz', verbose=verbose)
        if mask:
            filename_mask = filename + '-mask'
            self.resources['NIFTI'].files[(self.type).lower()+'_brain_mask.nii.gz'].download(filename_mask+'.nii.gz', verbose=verbose)
            return filename+'.nii.gz', filename_mask+'.nii.gz'
        else:
            return filename+'.nii.gz'
    else:
        remove_dicoms_after = False
        if not(dicom_dir):
            dicom_dir = tempfile.TemporaryDirectory().name
            remove_dicoms_after = True
            if verbose:
                print('No dicom_dir specified, using temporary directory will be deleted when completed: '+ dicom_dir)
        self.resources['DICOM'].download_dir(dicom_dir,verbose=verbose)
        scanFiles = [f for f in glob.glob(dicom_dir+'/*/scans/*/resources/DICOM/files/*') if not('.gif' in f)]
        filename = os.path.abspath(filename)
        os.makedirs(os.path.split(filename)[0], exist_ok=True)
        with open(os.devnull,'w') as devnull:
            tst=subprocess.call([dcm2niix, '-b','n','-z','y','-x','n','-t','n','-m','n','-o',os.path.split(filename)[0],'-f',os.path.split(filename)[1], str(scanFiles[10])],stderr=devnull)        
            print(tst)
        files = glob.glob(filename+'*.nii.gz')
        fName = files[np.argmax([os.path.getsize(f) for f in files])]
        if remove_dicoms_after:
            shutil.rmtree(dicom_dir)
        if segmentation or ((segmentation!=False) and (self.type in  ['ENT', 'ENR', 'ENF','ENT','ENR','CAV','NET','NCT','NER','NCR','ENP','NCP','NEP'])):
            print('thresholding segmentation'+str(fName))
            img=sitk.ReadImage(fName)
            tmp = sitk.GetArrayFromImage(img)[1:-1,1:-1,1:-1].flatten()
            
            if 'BlackBurnedIn' in self.fields.keys() and str(self.fields['BlackBurnedIn']).lower()=='true':
                print('BlackBurnedIn')
                min_val = np.min(tmp)
                thresholdedImg=sitk.BinaryThreshold(img,min_val-0.5,min_val+0.5,1,0)  
            else:
                stats=sitk.StatisticsImageFilter()
                stats.Execute(img)
                max_val = stats.GetMaximum()
                thresholdedImg=sitk.BinaryThreshold(img,max_val-0.5,max_val+0.5,1,0)              
            thresholdedImg= sitk.Cast(thresholdedImg,sitk.sitkUInt8)
            sitk.WriteImage(thresholdedImg,fName)      
            return fName
        else:
            return fName
        
        
def get_transforms_from_t1c(self, filename, inverse = False, verbose = False, overwrite = False):
    '''Used in get_transforms function.
    Downlad transforms from xnat and output filenames. If inverse = True, inverse files will be downladed and whichtoinvert will be changed accordingly.
    '''
    
    if inverse:
        if not os.path.isfile(f'{filename}-t1c-to-mni-inverse.nii.gz') or overwrite:
            os.makedirs(os.path.split(os.path.abspath(f'{filename}-t1c-to-mni-inverse.nii.gz'))[0], exist_ok = True)
            self.selected_t1c.resources['transforms'].files['t1c_to_mni.mat'].download(f'{filename}-t1c-to-mni.mat', verbose = verbose)
            self.selected_t1c.resources['transforms'].files['t1c_to_mni_inverse.nii.gz'].download(f'{filename}-t1c-to-mni-inverse.nii.gz', verbose = verbose)
        files = [f'{filename}-t1c-to-mni.mat', f'{filename}-t1c-to-mni-inverse.nii.gz']
        whichtoinvert = [True, False]
        return files, whichtoinvert 
        
    else:
        if not os.path.isfile(f'{filename}-t1c-to-mni-warp.nii.gz') or not os.path.isfile(f'{filename}-t1c-to-mni.mat') or overwrite:
            os.makedirs(os.path.split(os.path.abspath(f'{filename}-t1c-to-mni.mat'))[0], exist_ok = True)
            self.selected_t1c.resources['transforms'].files['t1c_to_mni.mat'].download(f'{filename}-t1c-to-mni.mat', verbose = verbose)
            self.selected_t1c.resources['transforms'].files['t1c_to_mni_warp.nii.gz'].download(f'{filename}-t1c-to-mni-warp.nii.gz', verbose = verbose)
        files = [f'{filename}-t1c-to-mni-warp.nii.gz', f'{filename}-t1c-to-mni.mat']
        whichtoinvert = [False, False] 
        return files, whichtoinvert 
    

def get_transforms(self, filename = '', space = 'mni', inverse = False, verbose = False, overwrite = False): 

    '''Downloads nifti transforms default ./nii-transforms/{sessionID}-{scanID}-{transformftype}. Outputs transformlist and whichtoinvert list.
           space: [None, 'mni', 't1c', 'native']  - if specified downloads transform or inverse to said space. Defaults to mni.  
                                                    Mods can download transform to mni or t1c and inverse to t1c (defualt) or native
           inverse [True, False]        - if True: downloads .mat and inverse.nii.gz files in correct order.
           overwrite: [True, False]     - if False, use existing nifti file if already present.'''
    
    exp = self.xnat_session.experiments[self.data['image_session_ID']]
       
    if 'transforms' in exp.selected_t1c.resources:
        if not filename:
            filename = './nii-transforms/'+self.image_session_id + '-' + self.id 
        filename = filename.replace('.nii.gz','').replace(':','_')
        scantype = self.type
        
        if not inverse:         
            if scantype in ['T1w','T2w','FLR']:
                if 'transforms' in self.resources:
                    if not os.path.isfile(f'{filename}-{scantype.lower()}-to-t1c.mat') or overwrite:
                        os.makedirs(os.path.split(os.path.abspath(f'{filename}-{scantype}-to-t1c.mat'))[0], exist_ok = True)
                        self.resources['transforms'].files[scantype.lower()+'_to_t1c.mat'].download(f'{filename}-{scantype.lower()}-to-t1c.mat', verbose = verbose)

                    if space.lower() == 't1c':
                        return [f'{filename}-{scantype.lower()}-to-t1c.mat'], [False]
                    else:
                        MNItransformlist, whichtoinvert = exp.get_transforms_from_t1c(filename, verbose = verbose, overwrite = overwrite)
                        return MNItransformlist + [f'{filename}-{scantype.lower()}-to-t1c.mat'], whichtoinvert + [False]
                else:
                    print ('no transforms to T1c found')
                    return


            elif scantype in ['T1c', 'ENT', 'ENR', 'ENF','ENT','ENR','CAV','NET','NCT','NER','NCR','ENP','NCP','NEP'] or '_autoseg' in scantype:
                MNItransformlist, whichtoinvert = exp.get_transforms_from_t1c(filename, verbose = verbose, overwrite = overwrite)
                return MNItransformlist, whichtoinvert
        else:
            MNItransformlist, whichtoinvert = exp.get_transforms_from_t1c(filename, inverse = True, verbose = verbose, overwrite = overwrite)
            if scantype in ['T1w','T2w','FLR'] and space == 'native':
                self.resources['transforms'].files[scantype.lower()+'_to_t1c.mat'].download(f'{filename}-{scantype.lower()}-to-t1c.mat', verbose = verbose)
                return [f'{filename}-{scantype.lower()}-to-t1c.mat']+MNItransformlist, [True]+whichtoinvert
            else:
                return MNItransformlist, whichtoinvert
            
    else:
        print('no transforms found')
        return       
    
        
def findREGfiles(scanFiles):
    warp = None
    inv_warp = None
    affine = None
    for scanFile in scanFiles:
        if not('inverse' in scanFile.lower()):
            if not('warp' in scanFile.lower()) and ('.mat' in scanFile.lower()):
                affine= scanFile
            elif ('warp' in scanFile.lower()):
                warp= scanFile
        else:
            inv_warp=scanFile
    return {'warp':warp,'affine':affine,'inv_warp':inv_warp}

def get_reg(self, output_dir='', verbose=False):
    tmp_output_dir = tempfile.TemporaryDirectory().name
    self.download_dir(tmp_output_dir,verbose=verbose)
    scanFiles = glob.glob(tmp_output_dir+'/*/*/*/*/*/*/*')
    output=findREGfiles(scanFiles)
    if output_dir:
        os.makedirs(output_dir,exist_ok=True)
        for k,v in output.items():
            
            shutil.move(v,output_dir+'/'+os.path.split(v)[-1])
            output[k] = output_dir+'/'+os.path.split(v)[-1]

    if not(os.path.isfile('./common/mya.mni0GenericAffine.mat')):
        tmpDir = tempfile.TemporaryDirectory().name
        self.xnat_session.projects['picture'].resources['common_files'].download_dir(tmpDir, verbose=verbose)
        for f in glob.glob(tmpDir+'/picture/resources/common_files/files/*'):
            print(f)
            shutil.copy(f,'./common/'+os.path.split(f)[-1])
        shutil.rmtree(tmpDir)        
    output['transforms_to_mni'] = []

    if 'dmu_def_reg' in output['affine']:
        output['transforms_to_mni'] += ['./common/mya.mni0GenericAffine.mat','./common/mya.mni1Warp.nii.gz']
    output['transforms_to_mni'] += [output['warp'],output['affine']]
    output['mni'] = './common/MNI152_T1_1mm.nii.gz'
    return output

@property
def is_preop(self):
    output = False
    if 'preop' in self.fields.keys():
        if str(self.fields['preop']).lower() == 'true':
            output = True
    return output
@property
def is_postop(self):
    output = False
    if 'postop' in self.fields.keys():
        if str(self.fields['postop']).lower() == 'true':
            output = True
    return output

@property
def fields(self):
    fields = {}
    for c in self.fulldata['children']:
        if c['field'] == 'parameters/addParam':
            for i in c['items']:
                fields[i['data_fields']['name']] = i['data_fields']['addField'].replace('&apos;',"'")
    return fields

def set_field(self, name, value):
    return self.set("parameters/addParam[name='"+name+"']/addField", value)
@property
def preop_experiment(self):
    preop_exp = [exp for exp in self.experiments.values() if exp.is_preop]
    assert len(preop_exp) in [0,1], 'more than 1 preop session found'
    if len(preop_exp)==1:
        return preop_exp[0]
    else:
        return None
@property
def postop_experiment(self):
    postop_exp = [exp for exp in self.experiments.values() if exp.is_postop]
    assert len(postop_exp) in [0,1], 'more than 1 postop session found'
    if len(postop_exp)==1:
        return postop_exp[0]
    else:
        return None
    
xnat.mixin.ExperimentData.selected_scan = get_sScan
xnat.mixin.ExperimentData.selected_t1c = selected_t1c
xnat.mixin.ExperimentData.selected_t1w = selected_t1w
xnat.mixin.ExperimentData.selected_t2w = selected_t2w
xnat.mixin.ExperimentData.selected_flr = selected_flr
xnat.mixin.ExperimentData.selected_reg = selected_reg
xnat.mixin.ExperimentData.segmentations = segmentations
xnat.mixin.ExperimentData.autosegmentations = autosegmentations
xnat.mixin.ExperimentData.exportobj = exportobj
xnat.mixin.ExperimentData.is_preop = is_preop
xnat.mixin.ExperimentData.is_postop = is_postop
xnat.mixin.ExperimentData.get_transforms_from_t1c = get_transforms_from_t1c

xnat.mixin.ImageSessionData.selected_scan = get_sScan
xnat.mixin.ImageSessionData.selected_t1c = selected_t1c
xnat.mixin.ImageSessionData.selected_t1w = selected_t1w
xnat.mixin.ImageSessionData.selected_t2w = selected_t2w
xnat.mixin.ImageSessionData.selected_flr = selected_flr
xnat.mixin.ImageSessionData.selected_reg = selected_reg
xnat.mixin.ImageSessionData.segmentations = segmentations
xnat.mixin.ImageSessionData.autosegmentations = autosegmentations
xnat.mixin.ImageSessionData.exportobj = exportobj
xnat.mixin.ImageSessionData.is_preop = is_preop
xnat.mixin.ImageSessionData.is_postop = is_postop

xnat.mixin.ImageScanData.get_nii = get_nii
xnat.mixin.ImageScanData.get_reg = get_reg
xnat.mixin.ImageScanData.get_transforms = get_transforms
#xnat.mixin.ImageScanData.fields = fields
xnat.mixin.ImageScanData.set_field = set_field
xnat.mixin.ImageScanData.parent_scan = parent_scan
xnat.mixin.SubjectData.preop_experiment = preop_experiment
xnat.mixin.SubjectData.postop_experiment = postop_experiment

